import java.util.*;

public class Demo {
  public static void main(String[] args) {
		//The normal way
		int a = 2;
		System.out.println("a = " + a);

		//Using print-assign
		int b .= 2;
			
		//The normal way
		b = 15;
		System.out.println("b = " + b);
		
		//Using print-assign
		a .= 15;
		
		//The possibilites are endless
		if ((a .= 1) == 1) {
			int[] c .= new int[] {1, 2, 3};
		}
  }
}
