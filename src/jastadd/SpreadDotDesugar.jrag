aspect SpreadDotDesugar {
  public TypeAccess TypeDecl.wrapInArrayWithSize(Expr exp) {
    return new ArrayTypeWithSizeAccess(createBoundAccess(), exp);
  }

  public TypeAccess ArrayDecl.wrapInArrayWithSize(Expr exp) {
    return new ArrayTypeAccess(componentType().wrapInArrayWithSize(exp));
  }

  syn nta Block SpreadDot.desugaredFor() {
    // We have T[] with method call that returns type R
    //
    // R[] newArr = new R[oldArr.length]
    // for (int i = 0; i < newArr.length; i++) {
    //    r[i] = oldArr[i].method(...);
    // }
    // return r;
    //
    // no return or assigment for return type = void though
    String iName = "i";
    String newArrName = "newArr";
    if (type().isVoid()) {
      // int i = 0;
      List<Modifier> iMods = new List<Modifier>(); // "i" prefix indicates index variable in for loop
      VariableDeclarator iDeclarator = new VariableDeclarator(iName, new List<Dims>(), new Opt(new IntegerLiteral(0)));
      PrimitiveTypeAccess iAccess = new PrimitiveTypeAccess("@primitive", "int");
      VarDeclStmt iDecl = new VarDeclStmt(new Modifiers(iMods), iAccess, new List<VariableDeclarator>(iDeclarator));

      // i < newArr.length;
      Dot newArrLength = new Dot(getLeft(), new VarAccess("length"));
      LTExpr ltExpr = new LTExpr(new VarAccess(iName), newArrLength);

      // i++
      ExprStmt incIStmt = new ExprStmt(new PostIncExpr(new VarAccess(iName)));

      // Statements
      List<Stmt> forBlockStmts = new List<Stmt>();
      Dot newArrAtIndexAccess = new Dot(getLeft(), new ArrayAccess(new VarAccess(iName)));
      Dot oldArrAtIndexAccess = new Dot(new ArrayAccess(new VarAccess(iName)), (MethodAccess) getRight());
      Dot oldArrAtIndexMethodCallAccess = new Dot(getLeft(), oldArrAtIndexAccess);
      ExprStmt call = new ExprStmt(oldArrAtIndexMethodCallAccess); //oldArr.voidMeth();
      forBlockStmts.add(call);

      Block forBlock = new Block(forBlockStmts);

      ForStmt forStmt = new ForStmt(new List<Stmt>(iDecl), new Opt(ltExpr), new List<Stmt>(incIStmt), forBlock);
      List<Stmt> stmts = new List<Stmt>();
      stmts.add(forStmt);

      return new Block(stmts);
    } else {
      // R[] newArr = new R[];
      Modifiers mods = new Modifiers(new List<Modifier>());
      Dot arrayLength = new Dot(getLeft(), new VarAccess("length"));
      TypeAccess arrayTypeWithSizeAccess = type().componentType().wrapInArrayWithSize(arrayLength);
      ArrayCreationExpr newArrCreatExpr = new ArrayCreationExpr(arrayTypeWithSizeAccess, new Opt());
      VariableDeclarator newArrDeclarator = new VariableDeclarator(newArrName, new List<Dims>(), new Opt(newArrCreatExpr));
      VarDeclStmt newArrDecl = new VarDeclStmt(mods, type().createBoundAccess(), new List<VariableDeclarator>(newArrDeclarator));

      // int i = 0;
      List<Modifier> iMods = new List<Modifier>(); // "i" prefix indicates index variable in for loop
      VariableDeclarator iDeclarator = new VariableDeclarator(iName, new List<Dims>(), new Opt(new IntegerLiteral(0)));
      PrimitiveTypeAccess iAccess = new PrimitiveTypeAccess("@primitive", "int");
      VarDeclStmt iDecl = new VarDeclStmt(new Modifiers(iMods), iAccess, new List<VariableDeclarator>(iDeclarator));

      // i < newArr.length;
      Dot newArrLength = new Dot(new VarAccess(newArrName), new VarAccess("length"));
      LTExpr ltExpr = new LTExpr(new VarAccess(iName), newArrLength);

      // i++
      ExprStmt incIStmt = new ExprStmt(new PostIncExpr(new VarAccess(iName)));

      // Statements
      List<Stmt> forBlockStmts = new List<Stmt>();
      Dot newArrAtIndexAccess = new Dot(new VarAccess(newArrName), new ArrayAccess(new VarAccess(iName)));
      Dot oldArrAtIndexAccess = new Dot(new ArrayAccess(new VarAccess(iName)), (MethodAccess) getRight());
      Dot oldArrAtIndexMethodCallAccess = new Dot(getLeft(), oldArrAtIndexAccess);
      ExprStmt assignToNewArr = new ExprStmt(new AssignSimpleExpr(newArrAtIndexAccess, oldArrAtIndexMethodCallAccess));
      forBlockStmts.add(assignToNewArr);

      Block forBlock = new Block(forBlockStmts);

      ForStmt forStmt = new ForStmt(new List<Stmt>(iDecl), new Opt(ltExpr), new List<Stmt>(incIStmt), forBlock);
      List<Stmt> stmts = new List<Stmt>(newArrDecl);
      stmts.add(forStmt);
      stmts.add(new ExprStmt(new VarAccess(newArrName)));

      return new Block(stmts);
    }
  }
  //arr *. toString() *. toString();
  public void SpreadDot.createBCode(CodeGeneration gen) {
    //System.err.println("Welcome to genbcode!");
    gen.emit(Bytecode.NOP);
    List<Stmt> s = desugaredFor().getStmts();

    if (type().isVoid()) {
      //System.err.println("Shouldnt be here");
      for (int i = 0; i < s.getNumChild(); i++) {
        s.getChild(i).createBCode(gen);
      }
    } else {
      //System.err.println("Generating bcode stmts " + s.getNumChild());
      for (int i = 0; i < s.getNumChild() - 1; i++) {
        s.getChild(i).createBCode(gen);
      }
      //System.err.println("Done generating bcode stmts");

      //System.err.println("Generating expr push");
      Expr var = ((ExprStmt) s.getChild(s.getNumChild() - 1)).getExpr();
      var.createBCode(gen);
      //System.err.println("Done generating expr push");
    }

    gen.emit(Bytecode.NOP);
    // gen.emit(Bytecode.NOP);
    //System.err.println("Goodbye to genbcode!");
  }
}
