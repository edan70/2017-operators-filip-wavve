package org.extendj;

import java.io.File;
import java.lang.ProcessBuilder;
import java.lang.ProcessBuilder.Redirect;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * This is a parameterized test: one test case is generated for each input
 * file found in TEST_DIRECTORY. Input files should have the ".in" extension.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * Edited by Filip & Wavve
 */
@RunWith(Parameterized.class)
public class CompileOutputTest {
	/** Directory where the test input files are stored. */
	private static final String TEST_DIR_STR = "testfiles/compile/";
	private static final File TEST_DIRECTORY = new File(TEST_DIR_STR);

	private final String filename;
	public CompileOutputTest(String testFile) {
		filename = testFile;
	}

	@Test
	public void test() throws Exception {
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", "compiler.jar", TEST_DIR_STR + filename);

		File outputFile = new File(TEST_DIRECTORY, Util.changeExtension(filename, ".out"));

		pb.redirectErrorStream(true);
		pb.redirectOutput(Redirect.to(outputFile));
		pb.start().waitFor();

		Util.compareOutput(outputFile,
				new File(TEST_DIRECTORY, Util.changeExtension(filename, ".expected")));
	}

	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return Util.getTestParameters(TEST_DIRECTORY, ".java");
	}
}
