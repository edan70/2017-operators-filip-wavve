public class TestSpreadDotVoidMethodError {
  static class Box {
    private int value;

    public Box(int value) {
      this.value = value;
    }

    public void incValue() {
      value++;
    }
  }

  public static void main(String[] args) {
    Box[] arr = new Box[] {new Box(1), new Box(2), new Box(3)};

    System.out.println(arr *. incValue());
  }
}
